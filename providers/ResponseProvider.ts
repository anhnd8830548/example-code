import { ApplicationContract } from '@ioc:Adonis/Core/Application'
import HttpStatus from 'http-status-codes'
import _ from 'lodash'

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the top level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the proviimport { Response } from '@adonisjs/core/build/standalone';
der class.
|
| @example:
|
| public async ready () {
|   const Database = this.app.container.resolveBinding('Adonis/Lucid/Database')
|   const Event = this.app.container.resolveBinding('Adonis/Core/Event')
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/

type ResponseData = {
  status: string
  message?: string
  code?: number
  data?: any
}

export default class ResponseProvider {
  public static needsApplication = true
  constructor(protected app: ApplicationContract) {}

  public register() {
    // Register your own bindings
  }

  public async boot() {
    const Response = this.app.container.use('Adonis/Core/Response')

    Response.macro('api', function (data, status) {
      const request = this.ctx!.request

      this.ctx!.response.header('Access-Control-Allow-Origin', '*')

      this.ctx!.response.status(status).json({
        response: {
          status: status,
          message: HttpStatus.getStatusText(status),
          url: request.completeUrl(),
        },
        data,
      })
      return this
    })
    
    Response.prototype.sendSuccess = function (data) {
      this.send({
        status: 'success',
        data,
      })
    }

    Response.prototype.sendError = function (
      code = 400,
      message = null,
      status = 400,
      data = null
    ) {
      const responseData: ResponseData = { status: 'error' }
      if (message) responseData.message = message
      if (code) responseData.code = code
      if (data) responseData.data = data
      this.status(status).send(responseData)
    }

    Response.prototype.sendDetailedError = function (obj = {}, contextCode) {
      const _error = _.defaults(obj, { status: 400, code: null, data: null, message: null })
      const { code, data, message } = _error
      this.status(_error.status).send({
        status: 'error',
        code,
        data,
        message,
      })
    }
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
