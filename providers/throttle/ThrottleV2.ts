import OwnError from 'App/Exceptions/OwnerError'
import { logger } from 'Config/logger'
import _ from 'lodash'
import { IThrottleLib, REQUEST_BANNED } from './Lib'

import { RateLimiterMemory, RateLimiterRes } from 'rate-limiter-flexible'

// Define configurations for different time windows
const config1 = {
  points: 10,
  duration: 60,
}

const config2 = {
  points: 15,
  duration: 120,
}

const config3 = {
  points: 15,
  duration: 3600,
  blockDuration: 3600, // Ban for 1 hour if limit is exceeded
}

// Create rate limiters with different configurations
const limiter1 = new RateLimiterMemory(config1)
const limiter2 = new RateLimiterMemory(config2)
const limiter3 = new RateLimiterMemory(config3)

// Function to handle requests
async function handleRequest(ip: string) {
  try {
    // Check if IP is allowed for the first time window
    await consumeFromLimiter(limiter1, ip)

    // Check if IP is allowed for the second time window
    await consumeFromLimiter(limiter2, ip)

    // Check if IP is allowed for the third time window
    await consumeFromLimiter(limiter3, ip)

    console.log('Request allowed')
  } catch (rejRes) {
    if (rejRes instanceof Error) {
      console.error(rejRes.message)
    } else {
      console.log('Request blocked')
    }
    throw new OwnError(REQUEST_BANNED, {})
  }
}

// Helper function to consume points from a limiter
async function consumeFromLimiter(
  limiter: RateLimiterMemory,
  key: string
): Promise<RateLimiterRes> {
  return limiter.consume(key)
}

export default class ThrottleV2 {
  throttle: IThrottleLib
  IS_PROD: boolean
  constructor(throttle: IThrottleLib, variables: any) {
    this.throttle = throttle
    const { Env } = variables
    logger.info(`AttlasThrottleMiddleware constructor env=${Env.get('NODE_ENV')}`)

    this.IS_PROD = Env.get('NODE_ENV') === 'production'
  }
  public async handle({ request, response, user }, next, params) {
    const ip =
      request.header('cf-connecting-ip') ||
      request.ip() ||
      request.header('x-forwarded-for') ||
      request.request.connection.remoteAddress
    try {
      await handleRequest(ip)
      return next()
    } catch {}
  }
}
