import OwnError from 'App/Exceptions/OwnerError'
import { logger } from 'Config/logger'
import _ from 'lodash'
import { IThrottleLib } from './Lib'

export default class Throttle {
  throttle: IThrottleLib
  IS_PROD: boolean
  constructor(throttle: IThrottleLib, variables: any) {
    this.throttle = throttle
    const { Env } = variables
    logger.info(`AttlasThrottleMiddleware constructor env=${Env.get('NODE_ENV')}`)

    this.IS_PROD = Env.get('NODE_ENV') === 'production'
  }
  public async handle({ request, response, user }, next, params) {
    if (!this.IS_PROD) {
      console.log('AttlasThrottleMiddleware arguments', _.omit(arguments, '0', '1'))
    }
    // Middleware: appthrottle:checkMode,30-60,90-120,ban,10-60,3600
    // params: ['checkMode', '30-60', '90-120', 'ban', '10-60', '3600']
    // checkMode: urlAndIp | urlAndUser | user
    let paramsParsed
    try {
      let checkMode = 'urlAndIp'
      let throttleParams: any = []
      let banParams: any = []
      let banIn

      let isBanSeparatorPassed = false
      params.forEach((e, i) => {
        if (e === 'ban') {
          isBanSeparatorPassed = true
        } else {
          let splitted = e.split('-')
          if (splitted.length !== 2 || !splitted.every((e) => +e)) {
            if (i === 0) {
              // May be checkMode
              checkMode = e
            } else if (i === params.length - 1 && isBanSeparatorPassed && +e) {
              banIn = +e
            } else {
              throw new Error('middleware params malformed')
            }
          } else {
            if (isBanSeparatorPassed) {
              banParams.push(splitted.map((e) => +e))
            } else {
              throttleParams.push(splitted.map((e) => +e))
            }
          }
        }
      })
      if (!throttleParams.length) {
        throttleParams = null
      }
      if (!banParams.length && !banIn) {
        banParams = null
      }
      let customKey: any = null
      if (checkMode === 'user' && user) {
        customKey = `user_${user.id}`
      } else if (checkMode === 'urlAndUser' && user) {
        customKey = `user_${user.id}_${request.method()}|${request.url()}`
      } else {
        // url and ip check
        customKey = null
      }
      paramsParsed = {
        checkMode,
        customKey,
        throttleParams,
        banParams,
        banIn: banIn || undefined,
      }
      if (!this.IS_PROD) {
        logger.info(
          `AttlasThrottleMiddleware: parsing params ${JSON.stringify(params)} =>`,
          paramsParsed
        )
      }
    } catch (e) {
      logger.error(`AttlasThrottleMiddleware parse error from: ${JSON.stringify(params)}`, e)
    }

    let passed = true
    if (paramsParsed) {
      const { customKey, throttleParams, banParams, banIn } = paramsParsed
      try {
        await this.throttle.onRequest(
          {
            request,
            response,
            customKey,
          },
          throttleParams,
          banParams,
          banIn
        )
      } catch (e) {
        passed = false
        const ip =
          request.header('cf-connecting-ip') ||
          request.ip() ||
          request.header('x-forwarded-for') ||
          request.request.connection.remoteAddress
        logger.error(
          `AttlasThrottleMiddleware: too many request on ${request.method()} | ${request.url()} | ${
            ip || 'unknown_ip'
          } | user=${_.get(user, 'id', '--')} ||`,
          _.get(e, 'message', e)
        )
        if (e instanceof OwnError) {
          if (e.headerWrites) {
            Object.keys(e.headerWrites).forEach((key) => {
              response.header(key, e.headerWrites[key])
            })
          }
          response.sendDetailedError(e)
        } else {
          response.sendError()
        }
      }
    } else {
      logger.warn(`AttlasThrottleMiddleware: params not parsed: ${JSON.stringify(params)}`)
    }
    if (passed) {
      return next()
    }
  }
}
