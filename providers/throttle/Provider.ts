import type { ApplicationContract } from '@ioc:Adonis/Core/Application'
import MemoryThrottle from './Lib'
import RedisThrottle from './Lib/throttleRedis'

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the top level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the proviimport { Config } from '@ioc:Adonis/Core/Config';
der class.import default from '../../config/error';

|
| @example:
|
| public async ready () {
|   const Database = this.app.container.resolveBinding('Adonis/Lucid/Database')
|   const Event = this.app.container.resolveBinding('Adonis/Core/Event')
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/

export default class Provider {
  constructor(protected app: ApplicationContract) {}

  public register() {
    // Register your own bindings
    this.app.container.singleton('Addons/Throttle', () => {
      const opts = {
        points: 6, // 6 points
        duration: 1, // Per second
      }
      const throttleLib = new RedisThrottle()
      const Throttle = require('./Throttle').default
      return new Throttle(throttleLib, { Config: this.app.config, Env: this.app.env })
    })
  }

  public async boot() {
    // All bindings are ready, feel free to use them
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
