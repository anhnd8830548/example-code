import { IRateLimiterOptions, RateLimiterMemory, RateLimiterRes } from 'rate-limiter-flexible'
import { IThrottleLib, REQUEST_BANNED, TOO_MANY_REQUESTS, makeKey } from '.'
import OwnError from 'App/Exceptions/OwnerError'
import crypto from 'crypto'

// Helper function to consume points from a limiter
async function consumeFromLimiter(
  limiter: RateLimiterMemory,
  key: string
): Promise<RateLimiterRes> {
  return limiter.consume(key)
}

const POINT_INDEX = 0
const DURATION_INDEX = 1

function hashArrays(throttleArray: number[], banArray: number[], banInSeconds: number): string {
  // Combine throttleArray and banArray into a single array for hashing
  const combinedArray = [...(throttleArray || []), ...(banArray || [])]

  // Convert the combined array to a string representation
  const combinedArrayString = JSON.stringify(combinedArray)

  // Create a hash using the SHA-256 algorithm
  const hash = crypto.createHash('sha256')

  // Update the hash with the string representation of the combined array
  hash.update(combinedArrayString)

  // Update the hash with the banInSeconds value
  hash.update(banInSeconds.toString())

  // Get the hexadecimal representation of the hash
  const hashedValue = hash.digest('hex')

  return hashedValue
}

interface LimiterStorage {
  [key: string]: {
    throttle: RateLimiterMemory[]
    ban: RateLimiterMemory[]
  }
}
class RedisThrottle implements IThrottleLib {
  limiterStorage: LimiterStorage

  constructor() {
    this.limiterStorage = {}
  }
  /**
   * Khi yêu cầu được gửi lên sẽ gọi vào hàm này
   *
   * @param options định danh request
   * @param throttleArray array chứa thông tin limit request, ví dụ [[60, 60], [120, 3600]] nghĩa là
   * - limit 60 request trong 60s
   * - limit 120 request trong 3600s (1h)
   * @param banArray array chứa thông tin để ban, cú pháp như throttleArray
   * @param banInSeconds request will be ban by this period if hit limit of banArray
   */
  async onRequest(options, throttleArray: number[], banArray: number[], banInSeconds = 3600) {
    const { uid, request, customKey } = options
    const KEY = makeKey({
      uid,
      request,
      customKey,
    })
    const keyArraysThrottleHash = hashArrays(throttleArray, banArray, banInSeconds)
    if (!(keyArraysThrottleHash in this.limiterStorage)) {
      // Create and assign a new entry in limiterStorage
      const throttleLimiters = throttleArray.map((throttle) => {
        const config: IRateLimiterOptions = {
          points: throttle[POINT_INDEX], // Assuming POINT_INDEX is 0
          duration: throttle[DURATION_INDEX], // Assuming DURATION_INDEX is 1
        }
        return new RateLimiterMemory(config)
      })

      const banLimiters = banArray.map((ban) => {
        const config: IRateLimiterOptions = {
          points: ban[POINT_INDEX], // Assuming POINT_INDEX is 0
          duration: ban[DURATION_INDEX], // Assuming DURATION_INDEX is 1
          blockDuration: banInSeconds,
        }
        return new RateLimiterMemory(config)
      })

      this.limiterStorage[keyArraysThrottleHash] = {
        throttle: throttleLimiters,
        ban: banLimiters,
      }
    }

    try {
      for (const throttle of this.limiterStorage[keyArraysThrottleHash].throttle) {
        const result = await consumeFromLimiter(throttle, KEY)
        console.log('Throttle Result:', result)
      }
    } catch {
      throw new OwnError(TOO_MANY_REQUESTS)
    }

    try {
      for (const ban of this.limiterStorage[keyArraysThrottleHash].ban) {
        const result = await consumeFromLimiter(ban, KEY)
        console.log('Ban Result:', result)
      }
    } catch {
      throw new OwnError(REQUEST_BANNED)
    }
  }
}

export default RedisThrottle
