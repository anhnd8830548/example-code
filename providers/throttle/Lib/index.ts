import Logger from '@ioc:Adonis/Core/Logger'
import LRUCache from 'lru-cache'
import { v4 as uuidv4 } from 'uuid'
import ms from 'ms'
import OwnError from 'App/Exceptions/OwnerError'
import { RateLimiter } from 'limiter'
import crypto from 'crypto'
import _ from 'lodash'

const TAG = `MemoryThrottle`
function makeKey(options) {
  const { uid, request, customKey } = options
  if (customKey) {
    return customKey
  }

  let keyRaw
  if (request) {
    const ip =
      request.header('cf-connecting-ip') ||
      request.ip() ||
      request.header('x-forwarded-for') ||
      request.request.connection.remoteAddress
    keyRaw = `${request.method()}|${request.url()}|${ip || 'unknown'}`
  }
  if (uid) {
    keyRaw += `:uid_${uid}`
  }
  if (!keyRaw) {
    const obj: any = {}
    Error.captureStackTrace(obj)
    Logger.warn(`${TAG} Cannot initiate key`, obj.stack)
    keyRaw = uuidv4()
  }

  const hashGen = crypto.createHash('sha1')
  hashGen.update(keyRaw)
  return hashGen.digest('hex')
}

interface IThrottleLib {
  onRequest(options, throttleArray, banArray, banInSeconds)
}

interface ICacheProvider {
  get(key: string, options?: any)
  set(key: string, value: string | boolean, options?: any)
  delete(key: string)
  getRemainingTTL(key: string): number
}

class RedisCacheAdapter implements ICacheProvider {
  public get(key: string, options?: any) {}

  public set(key: string, value: string | boolean, options?: any) {}

  public delete(key: string) {}

  public getRemainingTTL(key: string): number {
    return 0
  }
}

class MemoryThrottle implements IThrottleLib {
  cache: ICacheProvider
  constructor(options) {
    this.cache = new LRUCache(
      _.assign(
        {
          max: 1e6,
          ttl: ms('1h'),
          maxAge: ms('1h'),
          updateAgeOnGet: true,
          ttlAutopurge: true,
        },
        options.lruOptions
      )
    )
  }

  /**
   * Khi yêu cầu được gửi lên sẽ gọi vào hàm này
   *
   * @param options định danh request
   * @param throttleArray array chứa thông tin limit request, ví dụ [[60, 60], [120, 3600]] nghĩa là
   * - limit 60 request trong 60s
   * - limit 120 request trong 3600s (1h)
   * @param banArray array chứa thông tin để ban, cú pháp như throttleArray
   * @param banInSeconds request will be ban by this period if hit limit of banArray
   */
  async onRequest(options, throttleArray, banArray, banInSeconds = 3600) {
    const { uid, request, customKey } = options
    const KEY = makeKey({
      uid,
      request,
      customKey,
    })

    // Check for ban
    const isBan = this.cache.get(`is_banned_${KEY}`, { updateAgeOnGet: false })
    if (isBan === true) {
      let retryAfter = Math.floor(this.cache.getRemainingTTL(`is_banned_${KEY}`) / 1000)
      throw new OwnError(
        REQUEST_BANNED,
        {
          unlockIn: retryAfter,
        },
        {
          'Retry-After': retryAfter,
        }
      )
    }

    // Get limiters
    let limiters = this.cache.get(KEY)
    if (!limiters) {
      limiters = makeLimiters(throttleArray)
      this.cache.set(KEY, limiters, {
        ttl: limiters.maxTtl,
        maxAge: limiters.maxTtl,
      })
    }

    // Remove token from limiters
    let passedLimiters = true
    for (let i = 0; i < limiters.length; i++) {
      const { limiter } = limiters[i]

      const remainingRequests = await limiter.removeTokens(1)
      if (remainingRequests < 0) {
        passedLimiters = false
        break
      }
    }

    // Check for ban
    if (!passedLimiters) {
      if (banArray) {
        // Get ban limiters
        const BAN_KEY = `ban_key_${KEY}`
        let passedLimiters = true
        if (banArray.length) {
          let limiters = this.cache.get(BAN_KEY)
          if (!limiters) {
            limiters = makeLimiters(banArray)
            this.cache.set(BAN_KEY, limiters, {
              ttl: limiters.maxTtl,
              maxAge: limiters.maxTtl,
            })
          }
          // Remove token from ban limiters
          for (let i = 0; i < limiters.length; i++) {
            const { limiter } = limiters[i]

            const remainingRequests = await limiter.removeTokens(1)
            if (remainingRequests < 0) {
              passedLimiters = false
              break
            }
          }
        } else {
          // Immediately ban
          passedLimiters = false
        }
        if (!passedLimiters) {
          // BANNED
          this.cache.set(`is_banned_${KEY}`, true, {
            ttl: banInSeconds * 1000,
            maxAge: banInSeconds * 1000,
          })
          this.cache.delete(KEY)
          this.cache.delete(BAN_KEY)
          throw new OwnError(REQUEST_BANNED, {
            unlockIn: banInSeconds,
          })
        }
      }
      throw new OwnError(TOO_MANY_REQUESTS)
    }
  }
}

function makeLimiters(throttleArray) {
  let maxTtl
  return throttleArray.map(([requestCount, intervalSeconds]) => {
    const intervalMs = intervalSeconds * 1000
    if (!maxTtl || intervalMs > maxTtl) {
      maxTtl = intervalMs
    }
    const limiter = new RateLimiter({
      tokensPerInterval: requestCount,
      interval: intervalMs,
      fireImmediately: true,
    })
    return {
      limiter,
      config: {
        requestCount,
        intervalSeconds,
        maxTtl,
      },
    }
  })
}

const TOO_MANY_REQUESTS = { status: 429, code: 429, message: 'TOO_MANY_REQUESTS' }
const REQUEST_BANNED = { status: 418, code: 418, message: 'REQUEST_BANNED' }

export { IThrottleLib, TOO_MANY_REQUESTS, REQUEST_BANNED, makeKey }

export default MemoryThrottle
