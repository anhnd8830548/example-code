import type { ApplicationContract } from '@ioc:Adonis/Core/Application'
import { ResponseConstructorContract } from '@ioc:Adonis/Core/Response'
import { HooksContract, ServerContract } from '@ioc:Adonis/Core/Server'
import KyberQueue from 'App/Queues/KyberQueue'
import SwapQueue from 'App/Queues/SwapQueue'

export default class AppProvider {
  constructor(protected app: ApplicationContract) {}

  public register() {
    // Register your own bindings
  }

  public async boot() {
    // IoC container is ready
    this.app.container.withBindings(
      ['Adonis/Core/Response'],
      (context: ResponseConstructorContract) => {
        context.macro('error', function error(errors: any, status = 400, code = null, data = null) {
          this.status(status).json({
            success: false,
            code: code || status,
            message: errors,
            data,
          })

          return this
        })

        context.macro('success', function success(data, status = 200, code = null) {
          this.status(200).json({
            success: true,
            code: code || status,
            data,
          })

          return this
        })
      }
    )
  }

  public async ready() {
    await SwapQueue.initConnection()
    await KyberQueue.initConnection()
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
