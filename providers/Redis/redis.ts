import { Redis as IORedis } from 'ioredis'
import Env from '@ioc:Adonis/Core/Env'
import _ from 'lodash'
import { Redis as redis } from 'ioredis'
import { logger } from 'Config/logger'

const DEFAULT_NAME = Env.get('REDIS_CONNECTION', 'local')
const CONFIG_PREFIX = 'redis'
const DEFAULT_REDIS_CONFIG = {
  reconnectOnError: (e) => {
    logger.error(`[Redis] connection error`, e)
    return true
  },
  keepAlive: 30e3,
}

export default class Redis {
  redis: IORedis
  Config
  private _pool: {}
  constructor(config) {
    this._pool = {}
    this.Config = config
  }

  connection(name) {
    if (!name) name = DEFAULT_NAME

    if (this._pool[name]) {
      return this._pool[name]
    }

    let config = this.Config.get(`${CONFIG_PREFIX}.${name}`)
    if (!config) {
      const defaultConnection = this.Config.get(`${CONFIG_PREFIX}.connection`) || DEFAULT_NAME
      config = this.Config.get(`${CONFIG_PREFIX}.${defaultConnection}`)
    }
    if (config) {
      config._name = name
    }
    config = _.merge(
      {},
      DEFAULT_REDIS_CONFIG,
      {
        connectionName: `${process.env.APP_NAME || 'NoAppName'}_${config._name}`,
      },
      config
    )

    /**
     * CREATE INSTANCE
     */
    const instance = new redis(config)
    customFn(this._pool, config, instance)
    logger.info('Connect to redis')
    /**
     * END
     */

    this._pool[name] = instance
    return instance
  }

  public async get(key: string) {
    return 'value' + key
  }
}
function subscribeSingle(pool, config, channel, onMessage) {
  // Prepare subscriber client
  const connectionName = config._name
  if (!connectionName) {
    throw new Error(
      `[Redis] subscribeSingle connectionName invalid, connName=${connectionName}, channel=${channel}`
    )
  }
  const subscriberConnName = `${connectionName}_subscriber`
  let subscriber = pool[subscriberConnName]
  if (!subscriber) {
    subscriber = this.duplicate()
    pool[subscriberConnName] = subscriber
    logger.info(`[Redis] Create subscriber connection for ${connectionName}`)
  }
  subscriber.subscribe(channel, (e) => {
    if (e) {
      logger.error(`[Redis] Error subscribe ${channel}`, e)
    } else {
      logger.info(`[Redis] Subscribed ${channel} successfully`)
    }
  })
  subscriber.on('message', (_channel, data) => {
    if (_channel === channel) {
      onMessage?.(data)
    }
  })
}
function customFn(pool, config, instance) {
  instance.subscribeSingle = function () {
    subscribeSingle.call(instance, pool, config, ...arguments)
  }
}
