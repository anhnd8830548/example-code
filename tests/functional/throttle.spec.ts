import { test } from '@japa/runner'
function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

test.group('Throttle', () => {
  // Write your test here
  test('test throttle 429', async ({ client }) => {
    const [_, response] = await Promise.all([
      await client.get('/test-throttle'),
      await client.get('/test-throttle'),
    ])

    response.assertStatus(429)
  })

  test('test throttle ban', async ({ client }) => {
    const [_, __] = await Promise.all([
      await client.get('/test-throttle'),
      await client.get('/test-throttle'),
    ])
    await sleep(3000)
    const [___, response] = await Promise.all([
      await client.get('/test-throttle'),
      await client.get('/test-throttle'),
    ])

    response.assertStatus(429)
  })
})
