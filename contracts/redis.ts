declare module '@ioc:Redis' {
  import Redis from 'providers/Redis/redis'
  const RedisCache: Redis
  export default RedisCache
}
