declare module '@ioc:Adonis/Core/Response' {
  interface ResponseContract {
    /**
     * Macro to handle successful response
     * @param  data   Data to be sent in response
     * @param  status status of the response
     * @param  code   code, if applicable. Will fallback to status if not provided
     */
    success(data: any, status?: number, code?: any): this

    /**
     * Macro to handle erroneous response
     * @param  errors Errors to be sent in response
     * @param  status status of the response
     * @param  code   code, if applicable. Will fallback to status if not provided
     * @param  data   Extra data to be sent in response, if required
     */

    error(errors: any, status?: number, code?: any, data?: any): this
    /**
     *
     * @param data
     */

    sendSuccess(data: any): this
    /**
     *
     * @param code
     * @param message
     * @param status
     * @param data
     */
    sendError(code: any, message: any, status?: number, data?: any): this

    /**
     *
     * @param obj
     * @param contextCode
     */
    sendDetailedError(obj: any, contextCode): this
  }
}
