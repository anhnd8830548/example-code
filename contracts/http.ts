declare module '@ioc:Adonis/Core/Server' {
  interface ServerContract {
    httpServer: typeof import('http').Server
  }
}
