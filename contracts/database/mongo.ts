declare module '@ioc:Mongoose' {
  import { Mongoose } from 'mongoose'
  const mongoDatabase: Mongoose
  export default mongoDatabase
}
