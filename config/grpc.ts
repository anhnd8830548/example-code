/**
 * Config source: https://git.io/JemcF
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

import Env from '@ioc:Adonis/Core/Env'

/*
|--------------------------------------------------------------------------
| Redis configuration
|--------------------------------------------------------------------------
|
| Following is the configuration used by the Redis provider to connect to
| the redis server and execute redis commands.
|
| Do make sure to pre-define the connections type inside `contracts/redis.ts`
| file for AdonisJs to recognize connections.
|
| Make sure to check `contracts/redis.ts` file for defining extra connections
*/
const GrpcConfig = {
  wallet: {
    host: Env.get('GRPC_HOST', 'local'),
    protoPath: 'example.proto',
    serviceName: 'Wallet',
  },
  notification: {
    host: Env.get('GRPC_HOST', 'local'),
    protoPath: 'example.proto',
    serviceName: 'Wallet',
  },
  binanceBroker: {
    host: Env.get('GRPC_HOST', 'local'),
    protoPath: 'example.proto',
    serviceName: 'Wallet',
  },
}

export default GrpcConfig
