import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  require('./swap')
}).prefix('/v1')
