import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  require('./swap')
  require('./users')
}).prefix('/api')
