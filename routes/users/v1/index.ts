import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  require('./user')
}).prefix('/v1')
