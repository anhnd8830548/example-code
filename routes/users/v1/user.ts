import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.get('/login', 'AuthController.login')
})
  .prefix('/auth')
  .namespace('App/Controllers/Http/Api/v1/User')
