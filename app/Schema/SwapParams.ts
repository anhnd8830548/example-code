type SwapParams = {
  tokenIn: string
  tokenOut: string
  amount: number
}

export { SwapParams }
