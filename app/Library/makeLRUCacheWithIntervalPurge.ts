import LRUCache from 'lru-cache'
import _ from 'lodash'
import purgeLRUCacheInterval from './purgeLRUCacheInterval'

interface IntervalPurgeOptions {
  interval?: number
}

export default function makeLRUCacheWithIntervalPurge(
  lruOptions: any,
  nameForLogging: string | undefined,
  intervalPurgeOptions?: number | IntervalPurgeOptions
) {
  const cache = new LRUCache({
    ...lruOptions,
  })

  // Pick interval to purge
  let interval: number

  if (typeof intervalPurgeOptions === 'number') {
    interval = intervalPurgeOptions
  } else if (intervalPurgeOptions?.interval) {
    interval = intervalPurgeOptions.interval
  } else if (lruOptions?.maxAge) {
    const maxAge = lruOptions.maxAge
    interval = maxAge * 3
    interval = _.clamp(interval, 60e3, 15 * 60e3)
  } else {
    interval = 10 * 60e3
  }

  if (!nameForLogging) {
    // Get required filename
    const upperPath = module.parent?.filename
    const lastSlash = upperPath?.lastIndexOf('/')
    const filename = upperPath?.substring(lastSlash! + 1)

    // Suffix ttl
    let suffix = ''
    if (lruOptions?.maxAge) {
      const maxAge = lruOptions.maxAge
      suffix = `[${Math.floor(maxAge / 1e3)}s]`
    }

    nameForLogging = `${filename}${suffix}`
    console.warn(
      `makeLRUCacheWithIntervalPurge: nameForLogging is not provided (${upperPath}), auto generate ${nameForLogging}`
    )
  }

  const t = purgeLRUCacheInterval(cache, interval, nameForLogging)
  t.unref?.()

  return cache
}
