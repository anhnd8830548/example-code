import * as amqp from 'amqplib'
import Logger from '@ioc:Adonis/Core/Logger'
import _ from 'lodash'

interface AmqpOptions {
  TAG: string
  dontCreateChannel?: boolean
}

interface AmqpConnectArgs {
  queueUrl: string
  cb: (result: { channel?: amqp.Channel; connection?: amqp.Connection; error?: Error }) => void
  options: AmqpOptions
}

type Options = {
  TAG: string
  dontCreateChannel: boolean
}

async function amqpConnect(
  queueUrl: string,
  cb: any,
  options: AmqpOptions
): Promise<NodeJS.Timeout | undefined> {
  let { TAG, dontCreateChannel } = options
  TAG = `[RabbitMQ ${TAG}]`
  let connection: amqp.Connection, channel: amqp.Channel

  try {
    connection = await amqp.connect(queueUrl)
  } catch (e) {
    Logger.error(`${TAG} error on connect:`, _.get(e, 'message', '--'))
    return setTimeout(() => amqpConnect.apply(null, cb), 1000)
  }

  connection.on('error', function (err) {
    if (err.message !== 'Connection closing') {
      cb({
        error: err,
      })
      Logger.error(`${TAG} conn error`, err.message)
    }
  })

  connection.on('close', function () {
    Logger.error(`${TAG} reconnecting 🟡`)
    return setTimeout(() => amqpConnect.apply(null, cb), 1000)
  })

  // Create channel
  if (!dontCreateChannel) {
    channel = await connection.createChannel()
    Logger.info(`${TAG} connected with options (channel created) ✅`, JSON.stringify(options))
  } else {
    Logger.info(`${TAG} connected with options (no channel created) ✅`, JSON.stringify(options))
  }

  cb({
    channel,
    connection,
  })
}

export { amqpConnect }
