import _ from 'lodash'
import Logger from '@ioc:Adonis/Core/Logger'
import { LRUCache } from 'lru-cache'

function purgeLRUCacheInterval(
  cache: LRUCache<any, any>,
  interval: number,
  nameForLogging: string | undefined
) {
  // Random interval to avoid all cache purging at the same time
  let finalInterval = _.random(interval * 0.8, interval * 1.2, false)
  finalInterval = Math.max(1000, finalInterval)

  return setInterval(() => {
    const NOW = Date.now()
    nameForLogging && Logger.info(`purgeLRUCacheInterval ${nameForLogging} startToPurge…`)
    cache.purgeStale()
    const takes = Date.now() - NOW
    nameForLogging &&
      Logger.info(
        `purgeLRUCacheInterval [${
          takes > 150 ? 'WARNING_PURGE' : 'NORMAL'
        }] ${nameForLogging} purge done, takes ${takes}ms`
      )
  }, finalInterval)
}

export default purgeLRUCacheInterval
