import { Connection, Channel } from 'amqplib'
import Logger from '@ioc:Adonis/Core/Logger'
import { amqpConnect } from 'App/Library/amqpConnect'
import _ from 'lodash'
import Promise from 'bluebird'

import Config from '@ioc:Adonis/Core/Config'

type AmqpOptions = {
  TAG: string
}

type AmqpConnectResult = {
  channel?: Channel
  connection?: Connection
  error?: Error
}

type QueueType = 'publish' | 'consume' | 'both'

type InitConnectionOptions = {
  type?: QueueType
  connectionUrl?: string
}

type AddTaskSendOptions = {
  persistent?: boolean
}

type HandleTaskQueueOptions = {
  durable?: boolean
}

type HandleTaskConsumeOptions = {
  noAck?: boolean
}

const queueConnectionUrl = Config.get('app.queueConnectionUrl')

class BaseQueue {
  name = this.constructor.name || 'unknown'
  publishConnection?: Connection
  publishChannel?: Channel
  consumeConnection?: Connection
  consumeChannel?: Channel
  publishQueues?: object

  async initConnection(options: InitConnectionOptions = {}): Promise<void[]> {
    const { type, connectionUrl } = options
    const promises: Promise<void>[] = []

    if (type === 'publish' || type === 'both') {
      if (!this.publishConnection) {
        promises.push(
          new Promise<void>((resolve) => {
            amqpConnect(
              connectionUrl ?? queueConnectionUrl,
              async ({ channel, connection, error }: AmqpConnectResult) => {
                if (error) {
                  this.publishConnection = null
                  this.publishChannel = null
                  await this.onQueueError('publish', error)
                } else {
                  this.publishConnection = connection
                  this.publishChannel = channel
                  resolve()
                }
              },
              {
                TAG: `pub-${this.name || ''}`,
              }
            )
          })
        )
      }
    }

    if (type === 'consume' || type === 'both') {
      if (!this.consumeConnection) {
        promises.push(
          new Promise<void>((resolve) => {
            amqpConnect(
              connectionUrl ?? queueConnectionUrl,
              async ({ channel, connection, error }: AmqpConnectResult) => {
                if (error) {
                  this.consumeConnection = null
                  this.consumeChannel = null
                  await this.onQueueError('consume', error)
                } else {
                  this.consumeConnection = connection
                  this.consumeChannel = channel
                  await this.handleConsumerQueues()
                  resolve()
                }
              },
              {
                TAG: `sub-${this.name || ''}`,
              }
            )
          })
        )
      }
    }

    if (!promises.length) {
      Logger.warn(`Initiate connection for ${this.name} but type is invalid`)
    }

    const result = await Promise.all(promises)
    return result
  }

  async handleConsumerQueues(): Promise<void> {
    Logger.warn(`Calling handleConsumerQueues for ${this.name} but not implemented`)
  }

  async onQueueError(type: string, error: Error): Promise<void> {
    this.publishQueues = {}
    Logger.error(`Queue ${this.name} error on ${type}: not implemented`)
  }

  async ensureChannel(type: 'publish' | 'consume'): Promise<void> {
    let field: 'publishChannel' | 'consumeChannel'
    if (type === 'publish') {
      field = 'publishChannel'
    } else if (type === 'consume') {
      field = 'consumeChannel'
    } else {
      throw new Error(`[${this.name}] Invalid type ${type}`)
    }

    if (!this[field]) {
      let startAt = Date.now()
      await new Promise<void>((resolve, reject) => {
        const r = setInterval(() => {
          if (this[field]) {
            clearInterval(r)
            resolve()
          } else if (Date.now() - startAt > 30000) {
            clearInterval(r)
            reject(new Error(`${this.name} waiting for the channel to be established timed out`))
          }
        }, 1000)
      })
    }
  }

  async addTask(queue: string, msg: any, sendOptions: AddTaskSendOptions = {}): Promise<void> {
    msg = JSON.stringify(msg)
    await this.ensureChannel('publish')
    return this.publishChannel.sendToQueue(
      queue,
      Buffer.from(msg),
      _.defaults({ ...sendOptions }, { persistent: false })
    )
  }

  async handleTask(
    queue: string,
    prefetch: number,
    _queueOptions: HandleTaskQueueOptions = {},
    _consumeOptions: HandleTaskConsumeOptions = {}
  ): Promise<void> {
    try {
      await this.ensureChannel('consume')
      const consumeChannel = this.consumeChannel
      await consumeChannel.assertQueue(
        queue,
        Object.assign({ durable: false }, _queueOptions || {})
      )

      if (prefetch > 0) await consumeChannel.prefetch(prefetch)

      const consumeOptions = Object.assign({ noAck: false }, _consumeOptions || {})
      Logger.info(
        `--- ${this.name} --- Start consuming queue ${queue} ▶️`,
        JSON.stringify(consumeOptions)
      )

      return await consumeChannel.consume(
        queue,
        async (msg) => {
          if (msg === null) {
            return
          }

          let obj
          let msgContent = msg.content.toString()
          const {
            properties: { correlationId, replyTo },
          } = msg

          try {
            obj = JSON.parse(msgContent)
          } catch (err) {
            obj = msgContent
          }

          let result = await this.doTask(queue, obj, {
            fullMsg: msg,
          })

          if (!result) {
            result = {}
          }

          replyTo &&
            (await consumeChannel.sendToQueue(replyTo, Buffer.from(JSON.stringify(result)), {
              correlationId,
            }))

          if (consumeOptions.noAck !== true) {
            consumeChannel.ack(msg)
          }
        },
        consumeOptions
      )
    } catch (err) {
      Logger.error(`[${this.name}] q=${queue} handle task error`, err)
    }
  }

  async doTask(queue: string, obj: any, arg2: { fullMsg: any }): Promise<any> {
    throw new Error('Method not implemented.')
  }

  async cancelTask(tag: string): Promise<void> {
    await this.ensureChannel('consume')
    const ch = this.consumeChannel
    const cancelResult = await ch.cancel(tag)
    Logger.info(`Queue ${this.name} cancel task ${tag} with result`, cancelResult)
  }
}

export { BaseQueue }
export const Type = {
  PUBLISH: 'publish',
  CONSUME: 'consume',
}
