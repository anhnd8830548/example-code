import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'
import { BaseQueue } from './BaseQueue'
import makeLRUCacheWithIntervalPurge from 'App/Library/makeLRUCacheWithIntervalPurge'

const IS_PROD = process.env.NODE_ENV === 'production'

class BaseQueueWithAck extends BaseQueue {
  callbackFn = makeLRUCacheWithIntervalPurge(
    {
      ttl: 5 * 60e3,
    },
    `${this.name}-Cb`
  )
  defaultReplyToQueue: string

  async initConnection(options: any = {}): Promise<any> {
    if (options.type !== 'both') {
      throw new Error(`${this.name}: type must be both`)
    }
    await super.initConnection(options)
  }

  async handleConsumerQueues(): Promise<void> {
    throw new Error(`${this.name}: handleConsumerQueues must be overridden`)
  }

  async addTask(
    queue: string,
    msg: any,
    sendOptions: any,
    _replyToQueue: boolean | string = false,
    timeout: number = IS_PROD ? 90e3 : 10e3
  ): Promise<any> {
    let nameOfReplyToQueue: string | undefined, finalCorrId: string | undefined
    return new Promise(async (resolve) => {
      let replyToQueue: string | undefined
      if (!_replyToQueue) {
        // Leave it
      } else if (_replyToQueue === true) {
        replyToQueue = this.defaultReplyToQueue as string
        if (typeof replyToQueue !== 'string') {
          throw new Error(`${this.name}: defaultReplyToQueue not set`)
        }
      } else {
        replyToQueue = _replyToQueue as string
      }

      let correlationId: string | undefined
      if (replyToQueue) {
        if (!sendOptions || !sendOptions.correlationId) {
          correlationId = uuidv4()
        } else {
          correlationId = sendOptions.correlationId
        }
        nameOfReplyToQueue = replyToQueue
      }

      const options = Object.assign(
        {
          replyTo: replyToQueue,
          correlationId,
        },
        sendOptions || {}
      )

      if (options.correlationId && options.replyTo) {
        this.callbackFn.set(options.correlationId, (data: any) => {
          resolve(data)
        })
        finalCorrId = options.correlationId
        return super.addTask(queue, msg, options)
      } else {
        const result = await super.addTask(queue, msg, options)
        resolve(result)
      }
    })
  }

  async handleTask(
    queue: string,
    prefetch: number,
    _queueOptions: any = {},
    _consumeOptions: any = {}
  ): Promise<void> {
    const consumeOptions = Object.assign(
      {
        noAck: true,
      },
      _consumeOptions
    )
    return super.handleTask(queue, prefetch, _queueOptions, consumeOptions)
  }

  // Consumer handle Ack
  async doTask(queue: string, msg: any, moreData: any = {}): Promise<void> {
    if (msg == null) {
      return
    }
    const correlationId = _.get(moreData, 'fullMsg.properties.correlationId')
    if (correlationId) {
      this.callbackFn.get(correlationId)?.(msg)
      // this.callbackFn.delete(correlationId)
    }
  }
}

export default BaseQueueWithAck
