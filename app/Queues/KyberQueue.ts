import { SwapParams } from 'App/Schema/SwapParams'
import { BaseQueue } from './BaseQueue'
import axios from 'axios'
class KyberQueue extends BaseQueue {
  async initConnection() {
    await super.initConnection({
      type: 'consume',
    })
  }

  async handleConsumerQueues() {
    await this.handleTask(`swap`, 1000, { durable: true })
  }

  async doTask(queue, options: SwapParams) {
    try {
      console.log(options)
      const url = `https://aggregator-api.kyberswap.com/ethereum/api/v1/routes?tokenIn=${options.tokenIn}&tokenOut=${options.tokenOut}&amountIn=${options.amount}&saveGas=true&gasInclude`

      let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://aggregator-api.kyberswap.com/ethereum/api/v1/routes?tokenIn=0xdAC17F958D2ee523a2206206994597C13D831ec7&tokenOut=0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48&amountIn=1000000&saveGas=true&gasInclude',
        headers: {},
      }

      const response = await axios.request(config)
      return response.data // Return the data
    } catch (e) {
      console.error('doTask price stream', e)
    }
  }
}

export default new KyberQueue()
