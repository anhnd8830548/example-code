import { v4 as uuidv4 } from 'uuid'
import BaseQueueWithAck from './BaseQueueWithAck'

class SwapQueue extends BaseQueueWithAck {
  publishQueues = {}

  async initConnection() {
    return super.initConnection({
      type: 'both',
    })
  }

  async handleConsumerQueues() {
    const queueName = `stream_spot_read_queue:${uuidv4()}`
    await this.handleTask(queueName, 50, {
      exclusive: true,
    })
    this.defaultReplyToQueue = queueName
  }

  async getQueue(symbol) {
    await this.ensureChannel('publish')
    if (!this.publishQueues[symbol]) {
      const { queue: publishQueue } = await this.publishChannel.assertQueue(`swap`, {
        durable: true,
      })
      this.publishQueues[symbol] = publishQueue
    }
    return this.publishQueues[symbol]
  }

  async addTask(symbol, msg, options = {}, waitForAck = false) {
    const queue = await this.getQueue(symbol)
    if (!queue) {
      throw Error('Trader not allow')
    }
    return await super.addTask(queue, msg, options, waitForAck)
  }
}

export default new SwapQueue()
