import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import { schema } from '@ioc:Adonis/Core/Validator'
import grpc from '@ioc:Grpc'
import mongoDatabase from '@ioc:Mongoose'
import RedisCache from '@ioc:Redis'
import SwapQueue from 'App/Queues/SwapQueue'

export default class SwapsController {
  public async swap({ request, response }: HttpContextContract) {
    console.log(request.qs())
    RedisCache.connection('local')
    mongoDatabase
    // grpc.connection('wallet')
    // const newSwapSchema = schema.create({
    //   tokenIn: schema.string(),
    //   tokenOut: schema.string(),
    //   amount: schema.number(),
    // })
    const data = await SwapQueue.addTask(
      'test',
      {
        ...request.qs(),
        _type: 'read',
      },
      undefined,
      true
    )
    return response.sendSuccess(data)
  }
}
