import Config from '@ioc:Adonis/Core/Config'
interface AttlasErrorConfig {
  [key: string]: {
    status: number
    code: string
    message: string
  }
}
const AttlasError: AttlasErrorConfig = Config.get('error')

class OwnError extends Error {
  status: number
  data?: Record<string, any>
  headerWrites?: Record<string, any>

  constructor(
    messageObj: string | object,
    data?: Record<string, any>,
    headerWrites?: Record<string, any>
  ) {
    super()

    // Ensure the name of this error is the same as the class name
    this.name = this.constructor.name
    this.status = 400

    if (typeof messageObj === 'object') {
      Object.assign(this, messageObj)
    } else {
      const attlasError = AttlasError[messageObj]
      if (attlasError) {
        Object.assign(this, attlasError)
      }
      this.message = messageObj
    }

    if (data) {
      this.data = {
        ...(this.data || {}),
        ...data,
      }
    }

    if (headerWrites) {
      this.headerWrites = {
        ...(this.headerWrites || {}),
        ...headerWrites,
      }
    }

    // This clips the constructor invocation from the stack trace.
    // It's not absolutely essential, but it does make the stack trace a little nicer.
    // @see Node.js reference (bottom)
    Error.captureStackTrace(this, this.constructor)
  }
}
export default OwnError
