# Adonis API application Example code

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

- Step 1

copy file .env.example to .env

```bash
cp .env.example .env
```

- Step 2 run `npm install`
- Step 3 run `npm start` or run dev `npm dev`

### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

### Step by Step build a new Provider

- Step 1: create provider file using command:

```bash
node ace make:provider Logstash/index
```

It will be create file like this

```typescript
import type { ApplicationContract } from '@ioc:Adonis/Core/Application'
export default class LoggerProvider {
  constructor(protected app: ApplicationContract) {}

  public register() {
    // Register your own bindings
  }

  public async boot() {
    // All bindings are ready, feel free to use them
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
```

and save provider in .adonisrc.json

```typescript
  "providers": [
    ...other
    "./providers/Logstash/IndexProvider",
  ],
```

- Step 2: Informing TypeScript About the Binding

```typescript
// contracts/logger/.ts
declare module '@ioc:Logger/Discord' {
  import Logger from 'providers/LogProvider/discord'
  const DiscordLogger: Logger
  export default DiscordLogger
}
```

- Step 3: Import and Using

```typescript
import DiscordLogger from '@ioc:Logger/Discord'
```
